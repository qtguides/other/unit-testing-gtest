GOOGLETEST_DIR = D:/dev/QtProjects/googletest-master
include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += thread
CONFIG -= qt

HEADERS += \
        tst_testcase1.h \
    myincrementer.h \
    tst_testcase2.h \
    tst_testcase3.h

SOURCES += \
        main.cpp \
    myincrementer.cpp \
    tst_testcase3.cpp
