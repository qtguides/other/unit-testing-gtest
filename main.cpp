#include "tst_testcase1.h"
#include "tst_testcase2.h"
#include "tst_testcase3.h"
#include <gtest/gtest.h>

/*
 * Ver en el archivo .pro: la primer linea indica path de instalación del
 * framework googletest: clone o zip del proyecto.
 *
 * Ver googletest-master/googletest/docs/primer.rmd
 *
 * ASSERT_*: on error FATAL: abort current function test
 * EXPECT_*: on error FAILS: continue cuurent function test
 *
 * Esta aplicación se puede ejecutar de varias maneras:
 * . como aplicación (desde Qt)              : resultados se muestran en consola
 * . desde Tools -> Tests -> Run All Tests...: resultados se muestran en Qt
 * . desde solapa Test Results               : resultados se muestran en Qt
 */
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
