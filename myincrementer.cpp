#include "myincrementer.h"

MyIncrementer::MyIncrementer(int base)
{
    this->value = base;
}

void MyIncrementer::increment(int in)
{
    value += in;
}

int MyIncrementer::getValue()
{
    return this->value;
}
