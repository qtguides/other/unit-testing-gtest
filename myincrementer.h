#ifndef MYINCREMENTER_H
#define MYINCREMENTER_H


class MyIncrementer
{
public:
    MyIncrementer(int base);
    void increment(int in);
    int getValue();

private:
    int value;
};

#endif // MYINCREMENTER_H
