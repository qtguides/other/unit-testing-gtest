#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

using namespace testing;

/* Primeros pasos con gtest.
 * Hacemos un test que verifica que 1 == 1 y 0 == 0.
 * Notas:
 *   ASSERT_*: on error FATAL: abort current function test
 *   EXPECT_*: on error FAILS: continue cuurent function test
 *
 * Pruebe cambiando los valores a ver que error sucede.
 */
TEST(TestCase1, TestSet1)
{
    EXPECT_EQ(1, 1);
    ASSERT_THAT(0, Eq(0));
}

/* Primeros pasos con gtest.
 * Hacemos un test que verifica código que incrementa un valor.
 */
TEST(TestIncrement, Increment_by_5)
{
    int value = 100;
    int inc = 5;

    value += inc;

    EXPECT_EQ(value, 105);
}

/* Primeros pasos con gtest.
 * Hacemos un test que verifica código que incrementa un valor.
 */
TEST(TestIncrement, Increment_by_10)
{
    int value = 100;
    int inc = 10;

    value += inc;

    EXPECT_EQ(value, 110);
}
