#ifndef TST_TESTCASE2_H
#define TST_TESTCASE2_H

#include "myincrementer.h"

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

using namespace testing;

/*
 * En esta 2da parte vamos a hacer un test de una clase:
 *      MyIncrementer
 * Este test lo vamos a hacer en una "función de test".
 * Esta función instancia la clase, la usa, y verifica que funcione correctamente.
 */
TEST(TestIncrementClass, Increment_by_5)
{
    MyIncrementer inc(100);

    inc.increment(5);

    EXPECT_EQ(inc.getValue(), 105);
}

#endif // TST_TESTCASE2_H
