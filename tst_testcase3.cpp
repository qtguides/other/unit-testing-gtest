#include "tst_testcase3.h"

void Tst_TestCase3::SetUp() {
    std::cout << "setUp" << std::endl;
    inc = new MyIncrementer(100);
}
void Tst_TestCase3::TearDown() {
    std::cout << "TearDown" << std::endl;
    delete inc;
}

/* Esta no es una funcion, sino un metodo de la clase Tst_TestCase3 */
TEST_F(Tst_TestCase3, Increment_by_5)
{
    inc->increment(5);

    EXPECT_EQ(inc->getValue(), 105);
}
