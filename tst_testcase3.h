#ifndef TST_TESTCASE3_H
#define TST_TESTCASE3_H

#include "myincrementer.h"

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

using namespace testing;

/*
 * En esta 3ra parte vamos a hacer un test de una clase:
 *      MyIncrementer
 * Pero este test lo vamos a hacer en una "clase test".
 * Nuestra clase de test va a tener dos funciones:
 *     SetUp()   : inicializa todo lo que el test necesite. El framwork lo llama antes de iniciar el test.
 *     TearDown(): limpia los recursos que haya usado el test. El framwork lo llama al finalizar el test.
 * Luego en el .cpp se agregan las funciones de test (que esta vez serán metodos de la clase).
 */
class Tst_TestCase3 : public Test
{
public:
    void SetUp();
    void TearDown();
    MyIncrementer *inc;
};

#endif // TST_TESTCASE3_H
